package ru.baulina.tm.exception.empty;

public final class EmptyUserIdException extends RuntimeException{

    public EmptyUserIdException() {
        super("Error! UserId is empty...");
    }

}
