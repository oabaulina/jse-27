package ru.baulina.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.entity.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    List<User> findAll();

    @Nullable
    User findUser(@NotNull final String login, @NotNull final String password);

    @Nullable
    User findById(@NotNull final Long id);

    @Nullable
    User findByLogin(@NotNull final String login);

    void removeUser(@NotNull final User user);

    void removeById(@NotNull final Long id);

    void removeByLogin(@NotNull final String login);

}
