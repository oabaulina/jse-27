package ru.baulina.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface IPropertyService {

    @Nullable String getServiceHost();

    @Nullable Integer getServicePort();

    @Nullable String getSessionSalt();

    @Nullable Integer getSessionCycle();

    @Nullable String getJdbcUrl();

    @Nullable String getJdbcType();

    @Nullable String getJdbcHost();

    @Nullable Integer getJdbcPort();

    @Nullable String getJdbcName();

    @Nullable String getJdbcLogin();

    @Nullable String getJdbcPassword();

    @Nullable String getJdbcTimezone();

    @Nullable String getJdbcDriver();

    @Nullable String getJdbcDialect();

}
