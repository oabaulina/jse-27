package ru.baulina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.api.repository.IRepository;
import ru.baulina.tm.api.repository.ITaskRepository;
import ru.baulina.tm.api.service.ITaskService;
import ru.baulina.tm.entity.Project;
import ru.baulina.tm.entity.Task;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.exception.empty.*;
import ru.baulina.tm.exception.entity.TaskNotFoundException;
import ru.baulina.tm.exception.incorrect.IncorrectIndexException;
import ru.baulina.tm.repository.TaskRepository;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull private final PropertyService propertyService = new PropertyService();

    @NotNull private final EntityManagerFactoryService entityManagerFactoryService
            = new EntityManagerFactoryService(propertyService);

    @Override
    protected IRepository<Task> getRepository() {
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        return new TaskRepository(em);
    }

    @Nullable
    @Override
    public Task create(
            @Nullable final User user,
            @Nullable final Project project,
            @Nullable final String name
    ) {
        if (user == null) throw new EmptyUserException();
        if (project == null) throw new EmptyProjectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final Task task = new Task();
            task.setUser(user);
            task.setProject(project);
            task.setName(name);
            em.merge(task);
            em.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return null;
    }
    
    @Nullable
    @Override
    public Task create(
            @Nullable final User user, @Nullable final Project project,
            @Nullable final String name, @Nullable final String description
    ) {
        if (user == null) throw new EmptyUserException();
        if (project == null) throw new EmptyProjectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final Task task = new Task();
            task.setUser(user);
            task.setProject(project);
            task.setName(name);
            task.setDescription(description);
            em.merge(task);
            em.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return null;
    }

    @Override
    public void clear(@Nullable final Long userId) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ITaskRepository repository = new TaskRepository(em);
            repository.clear(userId);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable final Long userId) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ITaskRepository repository = new TaskRepository(em);
            @NotNull List<Task> listTask = repository.findAll(userId);
            em.getTransaction().commit();
            return listTask;
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return null;
    }

    @Nullable
    @Override
    public List<Task> findListTasks() {
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ITaskRepository repository = new TaskRepository(em);
            @NotNull List<Task> listTask = repository.findListTasks();
            em.getTransaction().commit();
            return listTask;
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return null;
    }

    @Nullable
    @Override
    public Task findOneById(
            @Nullable final Long userId, 
            @Nullable final Long projectId, 
            @Nullable final Long id
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (projectId == null || projectId < 0) throw new EmptyProjectIdException();
        if (id == null || id < 0) throw new EmptyIdException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ITaskRepository repository = new TaskRepository(em);
            @Nullable Task task = repository.findOneById(userId, projectId, id);
            em.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return null;
    }

    @Nullable
    @Override
    public Task findOneByIndex(
            @Nullable final Long userId,
            @Nullable final Long projectId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (projectId == null || projectId < 0) throw new EmptyProjectIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ITaskRepository repository = new TaskRepository(em);
            @Nullable Task task = repository.findOneByIndex(userId, projectId, index);
            em.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return null;
    }

    @Nullable
    @Override
    public Task findOneByName(
            @Nullable final Long userId,
            @Nullable final Long projectId,
            @Nullable final String name
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (projectId == null || projectId < 0) throw new EmptyProjectIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ITaskRepository repository = new TaskRepository(em);
            @Nullable Task task = repository.findOneByName(userId, projectId, name);
            em.getTransaction().commit();
            return task;
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return null;
    }

    @Override
    public void remove(
            @Nullable final Task task
    ) {
        if (task == null) throw new EmptyTaskException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ITaskRepository repository = new TaskRepository(em);
            repository.remove(task);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    public void removeOneById(
            @Nullable final Long userId,
            @Nullable final Long projectId,
            @Nullable final Long id
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (projectId == null || projectId < 0) throw new EmptyProjectIdException();
        if (id == null || id < 0) throw new EmptyIdException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ITaskRepository repository = new TaskRepository(em);
            @Nullable Task task = repository.removeOneById(userId, projectId, id);
            if (task == null) throw new TaskNotFoundException();
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    public void removeOneByIndex(
            @Nullable final Long userId,
            @Nullable final Long projectId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (projectId == null || projectId < 0) throw new EmptyProjectIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ITaskRepository repository = new TaskRepository(em);
            @Nullable Task task = repository.removeOneByIndex(userId, projectId, index);
            if (task == null) throw new TaskNotFoundException();
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    public void removeOneByName(
            @Nullable final Long userId,
            @Nullable final Long projectId,
            @Nullable final String name
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (projectId == null || projectId < 0) throw new EmptyProjectIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ITaskRepository repository = new TaskRepository(em);
            @Nullable Task task = repository.removeOneByName(userId, projectId, name);
            if (task == null) throw new TaskNotFoundException();
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    public void updateTaskById(
            @Nullable final Long userId, @Nullable final Long projectId,
            @Nullable final Long id,
            @Nullable final String name, @Nullable final String description
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (projectId == null || projectId < 0) throw new EmptyProjectIdException();
        if (id == null || id < 0) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            final Task task = findOneById(userId, projectId, id);
            if (task == null) throw new TaskNotFoundException();
            task.setId(id);
            task.setName(name);
            task.setDescription(description);
            em.merge(task);
            em.getTransaction().commit();
         } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    public void updateTaskByIndex(
            @Nullable final Long userId, @Nullable final Long projectId,
            @Nullable final Integer index,
            @Nullable final String name, @Nullable final String description
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (projectId == null || projectId < 0) throw new EmptyProjectIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final EntityManager em = entityManagerFactoryService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final ITaskRepository repository = new TaskRepository(em);
            @Nullable final Task task = findOneByIndex(userId, projectId, index);
            if (task == null) throw new TaskNotFoundException();
            task.setName(name);
            task.setDescription(description);
            repository.merge(task);
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

}
