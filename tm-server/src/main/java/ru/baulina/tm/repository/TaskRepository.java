package ru.baulina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.api.repository.ITaskRepository;
import ru.baulina.tm.entity.Task;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @NotNull private final EntityManager entityManager;

    public TaskRepository(@NotNull EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void remove(@NotNull final Task task) {
        @NotNull final Long id = task.getId();
        entityManager.remove(entityManager.find(Task.class, id));
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final Long userId) {
        @NotNull final String sql = "SELECT t FROM Task t WHERE t.user.id = :userId";
        @NotNull final TypedQuery<Task> query =
                entityManager.createQuery(sql, Task.class)
                        .setParameter("userId", userId);
        return query.getResultList();
    }

    @NotNull
    @Override
    public List<Task> findListTasks() {
        @NotNull final String sql = "SELECT t FROM Task t";
        @NotNull final TypedQuery<Task> query =
                entityManager.createQuery(sql, Task.class);
        return query.getResultList();
    }

    @Override
    public void clear(@NotNull final Long userId) {
        @NotNull final List<Task> tasks = findAll(userId);
        tasks.forEach(entityManager::remove);
    }

    @Nullable
    @Override
    public Task findOneById(
            @NotNull final Long userId,
            @NotNull final Long projectId,
            @NotNull final Long id
    ) {
        @NotNull final String sql = "SELECT t FROM Task t " +
                                    "WHERE t.user.id = :userId AND t.project.id = :projectId AND t.id = :id";
        @NotNull final TypedQuery<Task> query =
                entityManager.createQuery(sql, Task.class)
                        .setParameter("userId", userId)
                        .setParameter("projectId", projectId)
                        .setParameter("id", id);
        return query.getResultList().get(0);
    }

    @Nullable
    @Override
    public Task findOneByIndex(
            @NotNull final Long userId,
            @NotNull final Long projectId,
            @NotNull final Integer index
    ) {
        @NotNull final String sql = "SELECT t FROM Task t " +
                                    "WHERE t.user.id = :userId AND t.project.id = :projectId ORDER BY t.id";
        @NotNull final TypedQuery<Task> query =
                entityManager.createQuery(sql, Task.class)
                        .setParameter("userId", userId)
                        .setParameter("projectId", projectId);
        return query.getResultList().get(index);
    }

    @Nullable
    @Override
    public Task findOneByName(
            @NotNull final Long userId,
            @NotNull final Long projectId,
            @NotNull final String name
    ) {
        @NotNull final String sql = "SELECT t FROM Task t " +
                                    "WHERE t.user.id = :userId AND t.project.id = :projectId AND t.name = :name";
        @NotNull final TypedQuery<Task> query =
                entityManager.createQuery(sql, Task.class)
                        .setParameter("userId", userId)
                        .setParameter("projectId", projectId)
                        .setParameter("name", name);
        return query.getResultList().get(0);
    }

    @Nullable
    @Override
    public Task removeOneById(
            @NotNull final Long userId,
            @NotNull final Long projectId,
            @NotNull final Long id
    ) {
        @Nullable final Task task = findOneById(userId, projectId, id);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Nullable
    @Override
    public Task removeOneByIndex(
            @NotNull final Long userId,
            @NotNull final Long projectId,
            @NotNull final Integer index
    ) {
        @Nullable final Task task = findOneByIndex(userId, projectId, index);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Nullable
    @Override
    public Task removeOneByName(
            @NotNull final Long userId,
            @NotNull final Long projectId,
            @NotNull final String name
    ) {
        @Nullable final Task task = findOneByName(userId, projectId, name);
        if (task == null) return null;
        remove(task);
        return task;
    }

}
