package ru.baulina.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.endpoint.SessionDTO;

public final class DataXmlClearCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String name() {
        return "data-xml-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Delete xml data file.";
    }

    @Override
    public void execute() {
        System.out.println("[DATA XML DELETE]");
        @Nullable final SessionDTO session = getSession();
        endpointLocator.getAdminDampEndpoint().dataXmlClear(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
