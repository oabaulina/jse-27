package ru.baulina.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.endpoint.SessionDTO;

public final class DataBinarySaveCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String name() {
        return "data-bin-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to binary file.";
    }

    @Override
    public void execute() {
        System.out.println("[DATA BINARY SAVE]");
        @Nullable final SessionDTO session = getSession();
        endpointLocator.getAdminDampEndpoint().dataBinarySave(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
